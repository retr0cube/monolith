# 👾 Monolith

![GitLab forks](https://img.shields.io/gitlab/forks/retr0cube/monolith?color=purple&logo=gitlab&logoColor=white&style=for-the-badge) ![GitLab](https://img.shields.io/gitlab/license/retr0cube/monolith?color=purple&logo=markdown&style=for-the-badge)

An easy to use CLI for accessing Minecraft: Bedrock Edition Mods available in the Monolith website.

## ℹ Info

The project is in its infancy, so there are no instructions as of now.

## 📜 License
This repo is licensed under GNU GPLV3, under which you have:
- The right to download and run the software freely
- The right to make changes to the software as desired
- The right to redistribute copies of the software
- The right to modify and distribute copies of new versions of the software